+++
title = "About Me"
slug = "about"
thumbnail = "images/tn.png"
description = "about"
+++

---------------------------
## Introduction
A Bachelor of Computer Application (B.C.A) graduate with a passion for programming/web development/gamedev and Unix/Linux currently working as Oracle Application DBA.

Key technologies:
Oracle EBS,Linux and Bash, Javascript,Python, Java, SQL, PHP :)....

### Work Experience
Oracle Apps DBA at WIPRO.

---------------------------

## Other Activities 
A small indie gamedev working under the alias [EvolvedAnt](https://evolvedant.netlify.app/).
You can follow my gamedev journey in gamedev blog as well as in my itch profile.
* [EvolvedAnt Blog](https://evolvedant.netlify.app/)
* [Itch.io](https://evolvedantgames.itch.io/)

## Other Projects
* [Covid 19 Tracker](https://indcovid19.netlify.app/)


